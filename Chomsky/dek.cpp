int *fn(){
    int a = 1;
    int *mutat = &a;

    return mutat;
}

int main()
{
    //egész
    int a = 1;

    //egészre mutató mutató
    int b = 1;
    int *mutat = &b;

    //egész referenciája
    int c = 1;
    int &ref = c;

    //egészek tömbje
    int tomb[10];

    //egészek tömbjének referenciája (nem az első elemé)
    int (&reft)[10] = tomb;

    //egészre mutató mutatók tömbje
    int *mtomb[10];

    //egészre mutató mutatót visszaadó függvény
    int *(*fv)() = fn;

    //egészre mutató mutatót visszaadó függvényre mutató mutató
    void*(fn);
}
