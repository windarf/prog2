import std.ostream;

public class LZWBinFa {
	private Csomopont _fa;
	private int _melyseg;
	private int _atlagosszeg;
	private int _atlagdb;
	private double _szorasosszeg;
	protected Csomopont _gyoker;
	protected int _maxMelyseg;
	protected double _atlag;
	protected double _szoras;

	public LZWBinFa() {
		throw new UnsupportedOperationException();
	}

	public void _<(char aB) {
		throw new UnsupportedOperationException();
	}

	public void kiir() {
		throw new UnsupportedOperationException();
	}

	public void szabadit() {
		throw new UnsupportedOperationException();
	}

	public int getMelyseg() {
		return this._melyseg;
	}

	public double getAtlag() {
		return this._atlag;
	}

	public double getSzoras() {
		return this._szoras;
	}

	public void kiir(ostream aOs) {
		throw new UnsupportedOperationException();
	}

	private LZWBinFa(const_LZWBinFa aUnnamed_1) {
		throw new UnsupportedOperationException();
	}

	private LZWBinFa _(const_LZWBinFa aUnnamed_1) {
		throw new UnsupportedOperationException();
	}

	private void kiir(Csomopont aElem, ostream aOs) {
		throw new UnsupportedOperationException();
	}

	private void szabadit(Csomopont aElem) {
		throw new UnsupportedOperationException();
	}

	protected void rmelyseg(Csomopont aElem) {
		throw new UnsupportedOperationException();
	}

	protected void ratlag(Csomopont aElem) {
		throw new UnsupportedOperationException();
	}

	protected void rszoras(Csomopont aElem) {
		throw new UnsupportedOperationException();
	}
}