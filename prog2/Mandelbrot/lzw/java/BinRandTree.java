import std.ostream;

public class BinRandTree<ValueType extends typename> {
	protected BinRandTree.Node _root;
	protected BinRandTree.Node _treep;
	protected int _depth = {0};
	private Unirand _ur = {std::chrono::system_clock::now().time_since_epoch().count(), 0, 2};
	public BinRandTree.Node _unnamed_Node_;

	public BinRandTree(BinRandTree.Node aRoot, BinRandTree.Node aTreep) {
		throw new UnsupportedOperationException();
	}

	public BinRandTree(const_BinRandTree aOld) {
		throw new UnsupportedOperationException();
	}

	public BinRandTree.Node cp(BinRandTree.Node aNode, BinRandTree.Node aTreep) {
		throw new UnsupportedOperationException();
	}

	public BinRandTree _(const_BinRandTree aOld) {
		throw new UnsupportedOperationException();
	}

	public BinRandTree(BinRandTree aOld) {
		throw new UnsupportedOperationException();
	}

	public BinRandTree _(BinRandTree aOld) {
		throw new UnsupportedOperationException();
	}

	public void _BinRandTree() {
		throw new UnsupportedOperationException();
	}

	public BinRandTree _<(ValueType aValue) {
		throw new UnsupportedOperationException();
	}

	public void print() {
		throw new UnsupportedOperationException();
	}

	public void printr() {
		throw new UnsupportedOperationException();
	}

	public void print(BinRandTree.Node aNode, ostream aOs) {
		throw new UnsupportedOperationException();
	}

	public void print(const_Node aCnode, ostream aOs) {
		throw new UnsupportedOperationException();
	}

	public void deltree(BinRandTree.Node aNode) {
		throw new UnsupportedOperationException();
	}

	public int whereToPut() {
		throw new UnsupportedOperationException();
	}
	protected class Node {
		private ValueType _value;
		private BinRandTree.Node _left;
		private BinRandTree.Node _right;
		private int _count = {0};
		public BinRandTree.Node _unnamed_Node_;
		public BinRandTree _unnamed_BinRandTree_;

		private Node(const_Node aUnnamed_1) {
			throw new UnsupportedOperationException();
		}

		private BinRandTree.Node _(const_Node aUnnamed_1) {
			throw new UnsupportedOperationException();
		}

		private Node(BinRandTree.Node aUnnamed_1) {
			throw new UnsupportedOperationException();
		}

		private BinRandTree.Node _(BinRandTree.Node aUnnamed_1) {
			throw new UnsupportedOperationException();
		}

		public Node(ValueType aValue, int aCount) {
			throw new UnsupportedOperationException();
		}

		public ValueType getValue() {
			return this._value;
		}

		public BinRandTree.Node leftChild() {
			throw new UnsupportedOperationException();
		}

		public BinRandTree.Node rightChild() {
			throw new UnsupportedOperationException();
		}

		public void leftChild(BinRandTree.Node aNode) {
			throw new UnsupportedOperationException();
		}

		public void rightChild(BinRandTree.Node aNode) {
			throw new UnsupportedOperationException();
		}

		public int getCount() {
			return this._count;
		}

		public void incCount() {
			throw new UnsupportedOperationException();
		}
	}
}