#ifndef BINRANDTREE_H
#define BINRANDTREE_H

template <typename ValueType>
class BinRandTree {

protected:
	BinRandTree::Node* root;
	BinRandTree::Node* treep;
	int depth;
private:
	Unirand ur;

public:
	BinRandTree(BinRandTree::Node* root = nullptr, BinRandTree::Node* root = nullptr, BinRandTree::Node* treep = nullptr, BinRandTree::Node* treep = nullptr);

	BinRandTree(const BinRandTree& old);

	BinRandTree::Node* cp(BinRandTree::Node* node, BinRandTree::Node* node, BinRandTree::Node* treep, BinRandTree::Node* treep);

	BinRandTree(BinRandTree&& old);

	void ~BinRandTree();

	void print();

	void printr();

	void print(BinRandTree::Node* node, BinRandTree::Node* node, std::ostream& os, std::ostream& os);

	void print(const Node& cnode, std::ostream& os);

	void deltree(BinRandTree::Node* node, BinRandTree::Node* node);

	int whereToPut();

protected:
	class Node {

	private:
		ValueType value;
		BinRandTree::Node* left;
		BinRandTree::Node* right;
		int count;

		Node(const Node& unnamed_1);

		Node(BinRandTree::Node&& unnamed_1, BinRandTree::Node&& unnamed_1);

	public:
		Node(ValueType value, int count = 0);

		ValueType getValue();

		BinRandTree::Node* leftChild();

		BinRandTree::Node* rightChild();

		void leftChild(BinRandTree::Node* node, BinRandTree::Node* node);

		void rightChild(BinRandTree::Node* node, BinRandTree::Node* node);

		int getCount();

		void incCount();
	};
};

#endif
