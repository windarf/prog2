import java.io.BufferedReader; // Kell a buffer létrehozásához
import java.io.IOException; // Kivételkezeléshez
import java.io.InputStreamReader; // Beolvasás
import java.util.HashMap; //karaktertérkép létrehozásához
import java.util.regex.Matcher; //Összepárosító
import java.util.regex.Pattern; //Karakterminta

class leet {

    private String toLeetCode(String str) {
        Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
        StringBuilder result = new StringBuilder();
        HashMap<Character, String> map = new HashMap<Character, String>();

        map.put(’A’, "4");
        map.put(’B’, " ˘ A´ z");
        map.put(’C’, "Â©");
        map.put(’D’, "Ä‘");
        map.put(’E’, "â,\ensuremath{\lnot}");
        map.put(’F’, "´C’");
        map.put(’G’, "6");
        map.put(’H’, "#");
        map.put(’I’, "!");
        map.put(’J’, "Â˙ z");
        map.put(’K’, "X");
        map.put(’L’, "ÂŁ");
        map.put(’M’, "M");
        map.put(’N’, "r");
        map.put(’O’, "0");
        map.put(’P’, "p");
        map.put(’Q’, "0");
        map.put(’R’, "Â®");
        map.put(’S’, "$");
        map.put(’T’, "7");
        map.put(’U’, "Â$\mathrm{\mu}$");
        map.put(’V’, "v");
        map.put(’W’, "w");
        map.put(’X’, "%");
        map.put(’Y’, "Â ˛ A");
        map.put(’Z’, "z");
        map.put(’0’, "O");
        map.put(’1’, "I");
        map.put(’2’,"2");
        map.put(’3’,"2");
        map.put(’4’,"2");
        map.put(’5’,"2");
        map.put(’6’,"2");
        map.put(’7’,"2");
        map.put(’8’,"2");
        map.put(’9’,"2");

        for (int i = 0; i < str.length(); i++) {
        char key = Character.toUpperCase(str.charAt(i));
        Matcher matcher = pattern.matcher(Character.toString(key));
            if (matcher.find()) {
                result.append(key);
                result.append(’ ’);
            } else {
                result.append(map.get(key));
                result.append(’ ’);
                }
                }
                return result.toString();
    }
    public static void main(String[] args) throws IOException {
        leet obj = new leet();
        BufferedReader br = new BufferedReader(new InputStreamReader(System ←-
            .in));
        String leetWord;
        int cont;
        do {
                    System.out.println("\nEnter the English Words :-");
                    leetWord = br.readLine();
                    String leet = obj.toLeetCode(leetWord);
                    System.out.println("The 1337 Code is :- " + leet);
            System.out.println("\n\nDo you want to continue ? [1=Yes and 0= ←-
            No]");
            cont = Integer.parseInt(br.readLine());
        } while (cont != 0);
    }
}
            