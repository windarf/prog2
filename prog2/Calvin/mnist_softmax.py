from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse

# Import data
from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf
old_v = tf.logging.get_verbosity()
tf.logging.set_verbosity(tf.logging.ERROR)

import matplotlib.pyplot


FLAGS = None


def readimg():
    file = tf.read_file("sajat.png")
    img = tf.image.decode_png(file,1)
    return img

def main(_):
  mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)
  
  # Create the model
  x = tf.placeholder(tf.float32, [None, 784])
  W = tf.Variable(tf.zeros([784, 10]))
  b = tf.Variable(tf.zeros([10]))
  y = tf.matmul(x, W) + b
  mylist=[]
  ilist=[]
  # Define loss and optimizer
  y_ = tf.placeholder(tf.float32, [None, 10])

  # The raw formulation of cross-entropy,
  #
  #   tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(tf.nn.softmax(y)),
  #                                 reduction_indices=[1]))
  #
  # can be numerically unstable.
  #
  # So here we use tf.nn.softmax_cross_entropy_with_logits on the raw
  # outputs of 'y', and then average across the batch.
  cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = y_, logits = y))
  train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

  sess = tf.InteractiveSession()
  # Train
  tf.initialize_all_variables().run(session=sess)
  print("-- A halozat tanitasa")  
  for i in range(1000):
    batch = mnist.train.next_batch(50)
    batch_xs, batch_ys = mnist.train.next_batch(100)
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))  
    train_accuracy = accuracy.eval(feed_dict={
    x: batch[0], y_: batch[1]})
    sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
    mylist.append(train_accuracy)
    ilist.append(i)
    
    if i % 100 == 0:
      print(i/10, "%")
      
  
  # Test trained model
  print("-- A halozat tesztelese")  
  
  
   
  
  print("-- A MNIST 42. tesztkepenek felismerese, mutatom a szamot, a tovabblepeshez csukd be az ablakat")
  
  img = mnist.test.images[42]
  image = img

  matplotlib.pyplot.imshow(image.reshape(28, 28), cmap=matplotlib.pyplot.cm.binary)
  matplotlib.pyplot.savefig("4.png")  
  matplotlib.pyplot.show()

  classification = sess.run(tf.argmax(y, 1), feed_dict={x: [image]})

  print("-- Ezt a halozat ennek ismeri fel: ", classification[0])
  print("----------------------------------------------------------")

  print("-- A sajat kezi -asom felismerese, mutatom a szamot, a tovabblepeshez csukd be az ablakat")
  img = readimg()
  image = img.eval()
  image = image.reshape(28*28)
  matplotlib.pyplot.imshow(image.reshape(28,28), cmap=matplotlib.pyplot.cm.binary)
  matplotlib.pyplot.savefig("8.png")  
  matplotlib.pyplot.show()

  classification = sess.run(tf.argmax(y, 1), feed_dict={x: [image]})

  print("-- Ezt a halozat ennek ismeri fel: ", classification[0])
  print("----------------------------------------------------------")
  matplotlib.pyplot.plot(ilist,mylist, color='red', linestyle='solid', linewidth = 1,marker='o', markerfacecolor='blue', markersize=3)
        
  matplotlib.pyplot.show()

  
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--data_dir', type=str, default='/tmp/tensorflow/mnist/input_data',
                      help='Directory for storing input data')
  FLAGS = parser.parse_args()
  tf.app.run()
