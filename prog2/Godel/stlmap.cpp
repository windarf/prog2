#include <iostream>
#include <string>
#include <map>
#include <iomanip>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>

std::vector<std::pair<std::string, int>> sort_map ( std::map <std::string, int> &rank )
{
        std::vector<std::pair<std::string, int>> ordered;

        for ( auto & i : rank ) {
                if ( i.second ) {
                        std::pair<std::string, int> p {i.first, i.second};
                        ordered.push_back ( p );
                }
        }

        std::sort (
                std::begin ( ordered ), std::end ( ordered ),
        [ = ] ( auto && p1, auto && p2 ) {
                return p1.second > p2.second;
        }
        );

        return ordered;
}

int main(){

	std::map<std::string, int> map;
	map["elso"]=2;
	map["masodik"]=4;
	map["harmadik"]=1;
	map["negyedik"]=3;

	std::vector<std::pair<std::string, int>> megold = sort_map(map);

	for(auto & i: megold){
		std::cout<<i.first<<" "<<i.second << std::endl;
	}

}


//g++ stlmap.cpp -o stlmap -lboost_system -lboost_filesystem -lboost_program_options -lboost_date_time -std=c++14