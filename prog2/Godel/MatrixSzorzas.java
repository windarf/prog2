import java.util.Arrays;
import java.util.stream.IntStream;

public class MatrixSzorzas {
	public static void main(String[] args) {
		double[][] M1 = { { 3, 7 }, { 0, 1 }, { 2, 5 } };
		double[][] M2 = { { 4, 1 }, { 8, 3 } };
		
		double[][] eredmeny = Arrays.stream(M1).map(r ->
			IntStream.range(0, M2[0].length).mapToDouble(i ->
                IntStream.range(0, M2.length).mapToDouble(j -> 
                r[j] * M2[j][i]).sum()).toArray()).toArray(double[][]::new);

		System.out.println(Arrays.deepToString(eredmeny));
	}	
}
