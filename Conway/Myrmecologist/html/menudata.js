var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"Classes",url:"annotated.html",children:[
{text:"Class List",url:"annotated.html"},
{text:"Class Index",url:"classes.html"},
{text:"Class Hierarchy",url:"inherits.html"},
{text:"Class Members",url:"functions.html",children:[
{text:"All",url:"functions.html"},
{text:"Functions",url:"functions_func.html"},
{text:"Variables",url:"functions_vars.html"}]}]},
{text:"Files",url:"files.html",children:[
{text:"File List",url:"files.html"},
{text:"File Members",url:"globals.html",children:[
{text:"All",url:"globals.html",children:[
{text:"_",url:"globals.html#index__"},
{text:"a",url:"globals_a.html#index_a"},
{text:"l",url:"globals_l.html#index_l"},
{text:"m",url:"globals_m.html#index_m"},
{text:"q",url:"globals_q.html#index_q"},
{text:"u",url:"globals_u.html#index_u"}]},
{text:"Functions",url:"globals_func.html"},
{text:"Typedefs",url:"globals_type.html"},
{text:"Macros",url:"globals_defs.html",children:[
{text:"_",url:"globals_defs.html#index__"},
{text:"l",url:"globals_defs_l.html#index_l"},
{text:"q",url:"globals_defs_q.html#index_q"},
{text:"u",url:"globals_defs_u.html#index_u"}]}]}]}]}
